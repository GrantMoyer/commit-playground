use build_html::{Container, ContainerType, Html, HtmlContainer, HtmlPage};
use git2::{Commit, Repository};
use layout::{
	backends::svg::SVGWriter,
	core::{base::Orientation, color::Color, geometry::Point, style::StyleAttr},
	std_shapes::shapes::{Arrow, Element, ShapeKind},
	topo::layout::VisualGraph,
};
use std::{
	collections::HashMap,
	ffi::OsStr,
	fs::File,
	io::{stdin, Write},
	iter::once,
	path::Path,
	process::{Command, ExitStatus, Stdio},
};
use tempfile::{tempdir, TempDir};

struct GitRunner {
	work_dir: TempDir,
}

impl GitRunner {
	fn run<I, S>(&self, args: I) -> ExitStatus
	where
		I: IntoIterator<Item = S>,
		S: AsRef<OsStr>,
	{
		Command::new("git")
			.args(args)
			.current_dir(&self.work_dir)
			.stdin(Stdio::null())
			.status()
			.expect("Failed to execute git")
	}
}

enum Step {
	Command(String),
	Diagram { title: String, svg: String },
}

fn main() {
	// Remote isn't actually remote, it's just used as a git remote for the "local" repo
	let remote_dir = tempdir().expect("Failed to create temporary working directory");
	let remote_git = GitRunner {
		work_dir: remote_dir,
	};
	remote_git.run(["init", "--quiet", "--bare"]);
	let remote_repo =
		Repository::open(&remote_git.work_dir).expect("Failed to open temporary remote git repo");
	remote_repo
		.config()
		.expect("Failed to get remote repo config")
		.set_i32("core.abbrev", 4)
		.expect("Failed to set core.abbrev");

	let local_dir = tempdir().expect("Failed to create temporary working directory");
	let local_git = GitRunner {
		work_dir: local_dir,
	};
	local_git.run(["init", "--quiet"]);
	local_git.run([
		"remote".as_ref(),
		"add".as_ref(),
		"origin".as_ref(),
		remote_git.work_dir.as_ref(),
	]);
	let mut steps = Vec::new();
	let local_repo =
		Repository::open(&local_git.work_dir).expect("Failed to open temporary local git repo");
	local_repo
		.config()
		.expect("Failed to get local repo config")
		.set_i32("core.abbrev", 4)
		.expect("Failed to set core.abbrev");

	for line in stdin()
		.lines()
		.map(|l| l.expect("Failed to read line"))
		.map(|s| s.trim().to_string())
	{
		eprintln!("> {line}");
		if line.starts_with("git") {
			let mut args = shell_words::split(line.strip_prefix("git").unwrap())
				.expect("Failed to parse git command");
			if let Some("commit") = args.get(0).map(|s| s.as_str()) {
				args.insert(1, "--allow-empty".to_string());
			}
			local_git.run(args);
			steps.push(Step::Command(line));
		} else if let Some(command) = line.strip_prefix("!") {
			let mut args = shell_words::split(command)
				.expect("Failed to parse command")
				.into_iter();
			let mode = args.next().expect("Missing command mode");
			match mode.as_str() {
				"plot" => {
					let commits = walk_commits(&local_repo, args);
					let svg = plot_commits(&local_repo, commits);
					steps.push(Step::Diagram {
						title: command.to_string(),
						svg,
					});
				}
				"plot-all" => {
					let mut all_refs = local_repo
						.references()
						.expect("Failed to read local reference");
					let all_ref_names = all_refs
						.names()
						.map(|name| name.expect("Failed to read local reference name"));
					let commits = walk_commits(&local_repo, all_ref_names);
					let svg = plot_commits(&local_repo, commits);
					steps.push(Step::Diagram {
						title: command.to_string(),
						svg,
					});
				}
				"plot-remote" => {
					let commits = walk_commits(&remote_repo, args);
					let svg = plot_commits(&remote_repo, commits);
					steps.push(Step::Diagram {
						title: command.to_string(),
						svg,
					});
				}
				"plot-all-remote" => {
					let mut all_refs = remote_repo
						.references()
						.expect("Failed to read remote reference");
					let all_ref_names = all_refs
						.names()
						.map(|name| name.expect("Failed to read remote reference name"));
					let commits = walk_commits(&remote_repo, all_ref_names);
					let svg = plot_commits(&remote_repo, commits);
					steps.push(Step::Diagram {
						title: command.to_string(),
						svg,
					});
				}
				_ => panic!("Unrecognized mode: {mode}"),
			}
		}
	}

	write_output("out.html", steps);
}

fn write_output<P: AsRef<Path>>(filename: P, steps: Vec<Step>) {
	let mut page = HtmlPage::new().with_title("Commit Playground").with_style(
		r#"
			.commands {
				background-color: lightgrey;
				padding: 1ex;
				font-size: 1rem;
			}
			.diagram {
				display: flex;
				flex-direction: column;
				align-items: center;
			}
			.diagram p {
				margin: 0;
			}
			.diagram .scroll-box {
				align-self: start;
				overflow: auto;
				max-width: 100%;
			}
			.diagram text {
				font: initial;
			}
		"#,
	);
	let mut command_block = String::new();
	for step in steps {
		match step {
			Step::Command(command) => {
				command_block.push_str(&command);
				command_block.push('\n');
			}
			Step::Diagram { title, svg } => {
				if !command_block.is_empty() {
					page.add_preformatted_attr(command_block, once(("class", "commands")));
					command_block = String::new();
				}
				page.add_container(
					Container::new(ContainerType::Div)
						.with_attributes(once(("class", "diagram")))
						.with_paragraph(title)
						.with_container(
							Container::new(ContainerType::Div)
								.with_attributes(once(("class", "scroll-box")))
								.with_raw(svg),
						),
				);
			}
		}
	}
	if !command_block.is_empty() {
		page.add_preformatted_attr(command_block, once(("class", "commands")));
	}
	let mut f = File::create(filename).expect("Failed to open output file");
	f.write(page.to_html_string().as_bytes())
		.expect("Failed to write output file");
}

fn plot_commits<'a, I: IntoIterator<Item = Commit<'a>>>(repo: &Repository, commits: I) -> String {
	let commits = commits.into_iter().collect::<Vec<_>>();
	let mut graph = VisualGraph::new(Orientation::LeftToRight);
	let mut commit_handles = HashMap::new();
	for commit in commits.iter() {
		let commit_id = commit
			.as_object()
			.short_id()
			.expect("Failed to get short commit ID");
		let commit_id_str = commit_id.as_str().unwrap();
		let commit_summary = commit.summary().expect("Failed to read commit summary");
		let commit_abbrev_len = commit_summary
			.chars()
			.take(7)
			.map(|c| c.len_utf8())
			.sum::<usize>();
		let commit_abbrev = &commit_summary[..commit_abbrev_len];
		let node_label = if commit_abbrev_len < commit_summary.len() {
			format!("{commit_abbrev}…\n{commit_id_str}")
		} else {
			format!("{commit_abbrev}\n{commit_id_str}")
		};
		let node_shape = ShapeKind::new_box(&node_label);
		let node_style = StyleAttr::simple();
		let node_orientation = Orientation::LeftToRight;
		let node_size = Point::new(100.0, 50.0);
		let node = Element::create(node_shape, node_style, node_orientation, node_size);
		commit_handles.insert(commit.id(), graph.add_node(node));
	}
	for commit in commits {
		let commit_handle = commit_handles[&commit.id()];
		for parent in commit.parents() {
			if let Some(&parent_handle) = commit_handles.get(&parent.id()) {
				// Make edges from parent to child so graph is effectively right to left
				let mut arrow = Arrow::simple("");
				std::mem::swap(&mut arrow.start, &mut arrow.end);
				graph.add_edge(arrow, parent_handle, commit_handle);
			}
		}
	}
	for (branch, _) in repo
		.branches(None)
		.expect("Failed to get branches")
		.map(|e| e.expect("Failed to get branch"))
	{
		let node_label = branch
			.name()
			.expect("Failed too get branch name")
			.expect("Branch name is invalid utf-8");

		let commit_id = branch
			.get()
			.peel_to_commit()
			.expect("Failed to get commit from branch")
			.id();
		if let Some(&commit_handle) = commit_handles.get(&commit_id) {
			let node_shape = ShapeKind::new_box(node_label);
			let node_style = StyleAttr {
				line_color: Color::transparent(),
				..StyleAttr::simple()
			};
			let node_orientation = Orientation::LeftToRight;
			let node_size = Point::new(100.0, 25.0);
			let node = Element::create(node_shape, node_style, node_orientation, node_size);
			let branch_handle = graph.add_node(node);

			let mut arrow = Arrow::simple("");
			std::mem::swap(&mut arrow.start, &mut arrow.end);
			graph.add_edge(arrow, commit_handle, branch_handle);
		}
	}

	let mut svg = SVGWriter::new();
	if graph.num_nodes() > 0 {
		graph.do_it(false, false, false, &mut svg);
	}
	svg.finalize()
}

fn walk_commits<'a, I, S>(repo: &Repository, ranges: I) -> impl Iterator<Item = Commit> + '_
where
	I: IntoIterator<Item = S>,
	S: AsRef<str>,
{
	let mut revwalk = repo.revwalk().expect("Failed to create Revwalk for repo");
	for range in ranges {
		let revspec = repo
			.revparse(range.as_ref())
			.unwrap_or_else(|e| panic!("Failed to parse revspec {}: {e}", range.as_ref()));
		if revspec.mode().is_no_single() {
			revwalk.push(revspec.from().unwrap().id()).unwrap();
		} else if revspec.mode().is_merge_base() {
			let from = revspec.from().unwrap().id();
			let to = revspec.to().unwrap().id();
			revwalk.push(from).unwrap();
			revwalk.push(to).unwrap();
			let merge_base = repo.merge_base(from, to).unwrap_or_else(|e| {
				panic!(
					"Failed to find merge base for range {}: {e}",
					range.as_ref()
				)
			});
			revwalk.hide(merge_base).unwrap();
		} else if revspec.mode().is_range() {
			revwalk.hide(revspec.from().unwrap().id()).unwrap();
			revwalk.push(revspec.to().unwrap().id()).unwrap();
		} else {
			unreachable!();
		}
	}
	revwalk.map(|oid| {
		repo.find_commit(oid.expect("Failed to walk commits"))
			.expect("Failed to find commit")
	})
}
