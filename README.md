Commit Playground executes a list of git commands in real, temporary git repo. It sets up a "local" and "remote" repo, so remote git commands like pull and push can also be used. Its purpose is to make it easy to explore git's branching model and to generate instructive diagrams of git histories.

The commit-playground executable reads a list of git commands and inspection commands, one per line, on stdin. Git commands all start with "git" and look exactly like you would issue them on the command line. Inspection commands all start with "!", and some take arguments. It writes a file, *out.html*, containing a listing of executed commands alongside the output diagrams.

See the *examples* directory for examples of the syntax. For instance, *examples/push.txt*:

```
git commit -m A
git commit -m B
git push -u origin HEAD
git commit -m C
git commit -m D
!plot-all
!plot-all-remote
```

produces the output:

> ```
> git commit -m A
> git commit -m B
> git push -u origin HEAD
> git commit -m C
> git commit -m D
> ```
>
> plot-all
>
> ![plot-all output diagram](https://gitlab.com/GrantMoyer/commit-playground/-/raw/readme_images/plot-all.svg)
>
> plot-all-remote
>
> ![plot-all-remote output diagram](https://gitlab.com/GrantMoyer/commit-playground/-/raw/readme_images/plot-all-remote.svg)
